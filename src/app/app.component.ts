import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  correo=""
  placeholder = "Escribir Correo Electronico"
  condicion = false
  noValido = false

  constructor() { }

  verificar(val:any) {
    this.condicion = val.valid
    console.log(val);
    if (val.valid) {
      this.noValido = !val.valid
      this.condicion = val.valid
    } else {
      this.condicion = val.valid
      this.noValido = !val.valid
    }
    
  }

}
